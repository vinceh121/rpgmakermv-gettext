package me.vinceh121.rpgmvgt;

import java.util.List;

public class RPGEventStep {
	private int code, indent;
	private List<Object> parameters;

	public int getCode() {
		return this.code;
	}

	public void setCode(final int code) {
		this.code = code;
	}

	public int getIndent() {
		return this.indent;
	}

	public void setIndent(final int indent) {
		this.indent = indent;
	}

	public List<Object> getParameters() {
		return this.parameters;
	}

	public void setParameters(final List<Object> parameters) {
		this.parameters = parameters;
	}

	@Override
	public String toString() {
		return "RPGEventStep [code=" + this.code + ", indent=" + this.indent + ", parameters=" + this.parameters + "]";
	}

}
