package me.vinceh121.rpgmvgt;

import java.util.List;

public class RPGEvent {
	private int id, switchId, trigger;
	private String name;
	private List<RPGEventStep> list;

	public int getId() {
		return this.id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public int getSwitchId() {
		return this.switchId;
	}

	public void setSwitchId(final int switchId) {
		this.switchId = switchId;
	}

	public int getTrigger() {
		return this.trigger;
	}

	public void setTrigger(final int trigger) {
		this.trigger = trigger;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public List<RPGEventStep> getList() {
		return this.list;
	}

	public void setList(final List<RPGEventStep> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "RPGEvent [id="
				+ this.id
				+ ", switchId="
				+ this.switchId
				+ ", trigger="
				+ this.trigger
				+ ", name="
				+ this.name
				+ ", list="
				+ this.list
				+ "]";
	}
}
