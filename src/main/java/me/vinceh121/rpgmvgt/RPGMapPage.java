package me.vinceh121.rpgmvgt;

import java.util.List;

public class RPGMapPage {
	private Object conditions, image, moveRoute;
	private boolean directionFix, stepAnime, through, walkAnime;
	private int moveFrequency, moveSpeed, moveType, priorityType, trigger;
	private List<RPGEventStep> list;

	public Object getConditions() {
		return this.conditions;
	}

	public void setConditions(final Object conditions) {
		this.conditions = conditions;
	}

	public Object getImage() {
		return this.image;
	}

	public void setImage(final Object image) {
		this.image = image;
	}

	public Object getMoveRoute() {
		return this.moveRoute;
	}

	public void setMoveRoute(final Object moveRoute) {
		this.moveRoute = moveRoute;
	}

	public boolean isDirectionFix() {
		return this.directionFix;
	}

	public void setDirectionFix(final boolean directionFix) {
		this.directionFix = directionFix;
	}

	public boolean isStepAnime() {
		return this.stepAnime;
	}

	public void setStepAnime(final boolean stepAnime) {
		this.stepAnime = stepAnime;
	}

	public boolean isThrough() {
		return this.through;
	}

	public void setThrough(final boolean through) {
		this.through = through;
	}

	public boolean isWalkAnime() {
		return this.walkAnime;
	}

	public void setWalkAnime(final boolean walkAnime) {
		this.walkAnime = walkAnime;
	}

	public int getMoveFrequency() {
		return this.moveFrequency;
	}

	public void setMoveFrequency(final int moveFrequency) {
		this.moveFrequency = moveFrequency;
	}

	public int getMoveSpeed() {
		return this.moveSpeed;
	}

	public void setMoveSpeed(final int moveSpeed) {
		this.moveSpeed = moveSpeed;
	}

	public int getMoveType() {
		return this.moveType;
	}

	public void setMoveType(final int moveType) {
		this.moveType = moveType;
	}

	public int getPriorityType() {
		return this.priorityType;
	}

	public void setPriorityType(final int priorityType) {
		this.priorityType = priorityType;
	}

	public int getTrigger() {
		return this.trigger;
	}

	public void setTrigger(final int trigger) {
		this.trigger = trigger;
	}

	public List<RPGEventStep> getList() {
		return this.list;
	}

	public void setList(final List<RPGEventStep> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "RPGMapPage [conditions="
				+ this.conditions
				+ ", image="
				+ this.image
				+ ", moveRoute="
				+ this.moveRoute
				+ ", directionFix="
				+ this.directionFix
				+ ", stepAnime="
				+ this.stepAnime
				+ ", through="
				+ this.through
				+ ", walkAnime="
				+ this.walkAnime
				+ ", moveFrequency="
				+ this.moveFrequency
				+ ", moveSpeed="
				+ this.moveSpeed
				+ ", moveType="
				+ this.moveType
				+ ", priorityType="
				+ this.priorityType
				+ ", trigger="
				+ this.trigger
				+ ", list="
				+ this.list
				+ "]";
	}
}
