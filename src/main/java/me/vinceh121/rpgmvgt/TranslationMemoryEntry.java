package me.vinceh121.rpgmvgt;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_NULL)
public class TranslationMemoryEntry {
	private String source;
	private String target;
	@JsonProperty("source_language")
	private String sourceLanguage;
	@JsonProperty("target_language")
	private String targetLanguage;
	private String origin;
	private Long category;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getSourceLanguage() {
		return sourceLanguage;
	}

	public void setSourceLanguage(String sourceLanguage) {
		this.sourceLanguage = sourceLanguage;
	}

	public String getTargetLanguage() {
		return targetLanguage;
	}

	public void setTargetLanguage(String targetLanguage) {
		this.targetLanguage = targetLanguage;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public Long getCategory() {
		return category;
	}

	public void setCategory(Long category) {
		this.category = category;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (category ^ (category >>> 32));
		result = prime * result + ((origin == null) ? 0 : origin.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((sourceLanguage == null) ? 0 : sourceLanguage.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		result = prime * result + ((targetLanguage == null) ? 0 : targetLanguage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TranslationMemoryEntry other = (TranslationMemoryEntry) obj;
		if (category != other.category)
			return false;
		if (origin == null) {
			if (other.origin != null)
				return false;
		} else if (!origin.equals(other.origin))
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		if (sourceLanguage == null) {
			if (other.sourceLanguage != null)
				return false;
		} else if (!sourceLanguage.equals(other.sourceLanguage))
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		if (targetLanguage == null) {
			if (other.targetLanguage != null)
				return false;
		} else if (!targetLanguage.equals(other.targetLanguage))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TranslationMemoryEntry [source="
				+ source
				+ ", target="
				+ target
				+ ", sourceLanguage="
				+ sourceLanguage
				+ ", targetLanguage="
				+ targetLanguage
				+ ", origin="
				+ origin
				+ ", category="
				+ category
				+ "]";
	}
}
