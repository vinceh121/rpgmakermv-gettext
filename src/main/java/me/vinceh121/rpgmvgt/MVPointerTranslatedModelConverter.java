package me.vinceh121.rpgmvgt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import me.vinceh121.gettext.POEntry;

/**
 * Iteratively goes through every RPGEventStep from orig and match with the one
 * at the same JSON pointer in trans.
 * 
 * Issues:
 * - if other events are changed, no translation can be done.
 */
public class MVPointerTranslatedModelConverter {
	private final List<POEntry> entries = new Vector<>();

	private String filenameOrig = "", filenameTrans = "";
	private boolean ignoreUntranslatableCodes, ignoreMismatchedListSizes;

	public void addGroupsEntries(final List<RPGMapEventGroup> grpsOrig, final List<RPGMapEventGroup> grpsTrans) {
		this.entries.addAll(this.buildGroupsEntries(grpsOrig, grpsTrans));
	}

	public List<POEntry> buildGroupsEntries(final List<RPGMapEventGroup> grpsOrig,
			final List<RPGMapEventGroup> grpsTrans) {
		if (!this.ignoreMismatchedListSizes && grpsOrig.size() != grpsTrans.size()) {
			throw new IllegalStateException("Mismatched groups sizes: " + grpsOrig.size() + " != " + grpsTrans.size());
		}
		final List<POEntry> localEntries = new Vector<>();
		for (int i = 0; i < Math.min(grpsOrig.size(), grpsTrans.size()); i++) {
			final RPGMapEventGroup grpOrig = grpsOrig.get(i);
			final RPGMapEventGroup grpTrans = grpsTrans.get(i);
			localEntries.addAll(this.buildGroupEntries(grpOrig, grpTrans));
		}
		return localEntries;
	}

	public void addGroupEntries(final RPGMapEventGroup grpOrig, final RPGMapEventGroup grpTrans) {
		this.entries.addAll(this.buildGroupEntries(grpOrig, grpTrans));
	}

	public List<POEntry> buildGroupEntries(final RPGMapEventGroup grpOrig, final RPGMapEventGroup grpTrans) {
		if (!this.ignoreMismatchedListSizes && grpOrig.getPages().size() != grpTrans.getPages().size()) {
			throw new IllegalStateException(
					"Mismatched page sizes: " + grpOrig.getPages().size() + " != " + grpOrig.getPages().size());
		}
		final List<POEntry> localEntries = new Vector<>();
		for (int iPage = 0; iPage < Math.min(grpOrig.getPages().size(), grpTrans.getPages().size()); iPage++) {
			final RPGMapPage pageOrig = grpOrig.getPages().get(iPage);
			final RPGMapPage pageTrans = grpTrans.getPages().get(iPage);
			if (!this.ignoreMismatchedListSizes && pageOrig.getList().size() != pageTrans.getList().size()) {
				throw new IllegalStateException(
						"Mismatched page sizes: " + pageOrig.getList().size() + " != " + pageTrans.getList().size());
			}
			for (int iEvt = 0; iEvt < Math.min(pageOrig.getList().size(), pageTrans.getList().size()); iEvt++) {
				final RPGEventStep evtOrig = pageOrig.getList().get(iEvt);
				final RPGEventStep evtTrans = pageTrans.getList().get(iEvt);
				localEntries.addAll(this.buildEntries(grpOrig.getName(), grpOrig.getId(), iPage, evtOrig, evtTrans));
			}
		}
		return localEntries;
	}

	public void addEventEntries(final List<RPGEvent> origList, final List<RPGEvent> transList) {
		this.entries.addAll(this.buildEntries(origList, transList));
	}

	public List<POEntry> buildEntries(final List<RPGEvent> origList, final List<RPGEvent> transList) {
		if (!this.ignoreMismatchedListSizes && origList.size() != transList.size()) {
			throw new IllegalStateException(
					"Mismatched event lists sizes: " + origList.size() + " != " + transList.size());
		}
		final List<POEntry> localEntries = new Vector<>();
		for (int iEvt = 0; iEvt < Math.min(origList.size(), transList.size()); iEvt++) {
			final RPGEvent evtOrig = origList.get(iEvt);
			final RPGEvent evtTrans = transList.get(iEvt);
			if (evtOrig == null || evtTrans == null) {
				continue;
			}
			if (!this.ignoreMismatchedListSizes && evtOrig.getList().size() != evtTrans.getList().size()) {
				throw new IllegalStateException("Mismatched event step list sizes: "
						+ evtOrig.getList().size()
						+ " != "
						+ evtTrans.getList().size());
			}
			for (int iStep = 0; iStep < Math.min(evtOrig.getList().size(), evtTrans.getList().size()); iStep++) {
				final RPGEventStep stepOrig = evtOrig.getList().get(iStep);
				final RPGEventStep stepTrans = evtTrans.getList().get(iStep);
				// if (!TRANSLATABLE_TYPES.contains(step.getCode()))
				// continue;

				this.addEntry(evtOrig, evtTrans, iStep, stepOrig, stepTrans);
			}
		}
		return localEntries;
	}

	public void addEntry(final RPGEvent evtOrig, final RPGEvent evtTrans, final int iStep, final RPGEventStep stepOrig,
			final RPGEventStep stepTrans) {
		this.entries.addAll(this.buildEventEntries(evtOrig, evtTrans, iStep, stepOrig, stepTrans));
	}

	public List<POEntry> buildEventEntries(final RPGEvent evtOrig, final RPGEvent evtTrans, final int iStep,
			final RPGEventStep stepOrig, final RPGEventStep stepTrans) {
		if (evtOrig.getId() != evtTrans.getId()) {
			throw new IllegalStateException("Got events with different IDs: " + evtOrig + ", " + evtTrans);
		}
		return this.buildEntries(evtOrig.getName(), evtOrig.getId(), iStep, stepOrig, stepTrans);
	}

	public List<POEntry> buildEntries(final String name, final int id, final int iStep, final RPGEventStep stepOrig,
			final RPGEventStep stepTrans) {
		switch (stepOrig.getCode()) {
		case 102:
			this.checkSameCodes(stepOrig, stepTrans);
			return this.buildChoiceEntries(name, id, iStep, stepOrig, stepTrans);
		case 356:
			this.checkSameCodes(stepOrig, stepTrans);
		case 401:
			return List.of(this.buildSingleLine(name, id, iStep, stepOrig, stepTrans));
		default:
			if (!this.ignoreUntranslatableCodes) {
				throw new IllegalStateException("Don't know how to process event code " + stepOrig.getCode());
			} else {
				return Collections.emptyList();
			}
		}
	}

	private void checkSameCodes(final RPGEventStep stepOrig, final RPGEventStep stepTrans) {
		if (stepOrig.getCode() != stepTrans.getCode()) {
			throw new IllegalStateException(
					"Original and translated steps have different codes: " + stepOrig + ", " + stepTrans);
		}
	}

	private List<POEntry> buildChoiceEntries(final String name, final int id, final int iStep,
			final RPGEventStep stepOrig, final RPGEventStep stepTrans) {
		@SuppressWarnings("unchecked")
		final List<String> strsOrig = (List<String>) stepOrig.getParameters().get(0);
		@SuppressWarnings("unchecked")
		final List<String> strsTrans = (List<String>) stepTrans.getParameters().get(0);
		if (!this.ignoreMismatchedListSizes && strsOrig.size() != strsTrans.size()) {
			throw new IllegalStateException("Mismatched choice size: " + name + ", " + id + ", " + iStep);
		}

		final List<POEntry> localEntries = new ArrayList<>(strsOrig.size());
		for (int i = 0; i < Math.min(strsOrig.size(), strsTrans.size()); i++) {
			final String strOrig = strsOrig.get(i);
			final String strTrans = strsTrans.get(i);
			final POEntry entry = this.buildBaseEntry(name, id, iStep);
			((RPGReference) entry.getReferences().get(0)).setParameter(i);
			entry.setMsgId(strOrig);
			entry.setMsgStr(strTrans);
			localEntries.add(entry);
		}
		return localEntries;
	}

	private POEntry buildSingleLine(final String name, final int id, final int iStep, final RPGEventStep stepOrig,
			final RPGEventStep stepTrans) {
		final POEntry entry = this.buildBaseEntry(name, id, iStep);
		entry.setMsgId((String) stepOrig.getParameters().get(0));
		entry.setMsgStr((String) stepTrans.getParameters().get(0));
		return entry;
	}

	// private POEntry buildBaseEntry(RPGMapEventGroup evt, int iPage, int iEvt) {
	// POEntry entry = this.buildBaseEntry(evt.getName(), evt.getId(), iEvt);
	// ((RPGReference) entry.getReferences().get(0)).setPage(iPage);
	// return entry;
	// }
	//
	// private POEntry buildBaseEntry(RPGEvent evt, int iStep) {
	// return this.buildBaseEntry(evt.getName(), evt.getId(), iStep);
	// }

	private POEntry buildBaseEntry(final String name, final int id, final int iStep) {
		final POEntry entry = new POEntry();
		entry.setCommentExtracted(name);
		final RPGReference refOrig = new RPGReference();
		refOrig.setFilePath(this.filenameOrig);
		refOrig.setLine(0);
		refOrig.setEvent(id);
		refOrig.setStep(iStep);
		final RPGReference refTrans = new RPGReference();
		refTrans.setFilePath(this.filenameTrans);
		refTrans.setLine(0);
		refTrans.setEvent(id);
		refTrans.setStep(iStep);
		entry.getReferences().add(refOrig);
		entry.getReferences().add(refTrans);
		return entry;
	}

	public List<POEntry> getEntries() {
		return this.entries;
	}

	public String getFilenameOrig() {
		return this.filenameOrig;
	}

	public void setFilenameOrig(final String filenameOrig) {
		this.filenameOrig = filenameOrig;
	}

	public String getFilenameTrans() {
		return this.filenameTrans;
	}

	public void setFilenameTrans(final String filenameTrans) {
		this.filenameTrans = filenameTrans;
	}

	public boolean isIgnoreUntranslatableCodes() {
		return this.ignoreUntranslatableCodes;
	}

	public void setIgnoreUntranslatableCodes(final boolean ignoreUntranslatableCodes) {
		this.ignoreUntranslatableCodes = ignoreUntranslatableCodes;
	}

	public boolean isIgnoreMismatchedListSizes() {
		return this.ignoreMismatchedListSizes;
	}

	public void setIgnoreMismatchedListSizes(final boolean ignoreMismatchedListSizes) {
		this.ignoreMismatchedListSizes = ignoreMismatchedListSizes;
	}
}
