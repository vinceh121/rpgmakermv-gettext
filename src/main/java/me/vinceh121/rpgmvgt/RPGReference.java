package me.vinceh121.rpgmvgt;

import java.nio.file.Path;
import java.util.regex.Pattern;

import me.vinceh121.gettext.POReference;

/**
 * CommonEvents: event:step:parameter
 * Map: group:page:step:parameter
 */
public class RPGReference extends POReference {
	private ReferenceType type;
	private int[] parts = new int[4];

	public RPGReference() {
	}

	public int getEvent() {
		return parts[0];
	}

	public void setEvent(int event) {
		parts[0] = event;
	}

	public int getStep() {
		return parts[type == ReferenceType.COMMON_EVENTS ? 1 : 2];
	}

	public void setStep(int step) {
		parts[type == ReferenceType.COMMON_EVENTS ? 1 : 2] = step;
	}

	public int getParameter() {
		return parts[type == ReferenceType.COMMON_EVENTS ? 2 : 3];
	}

	public void setParameter(int parameter) {
		parts[type == ReferenceType.COMMON_EVENTS ? 2 : 3] = parameter;
	}

	public int getGroup() {
		return parts[0];
	}

	public void setGroup(int group) {
		parts[0] = group;
	}

	public int getPage() {
		return parts[1];
	}

	public void setPage(int page) {
		parts[1] = page;
	}

	public ReferenceType getType() {
		return type;
	}

	public void setType(ReferenceType type) {
		this.type = type;
	}

	public int[] getParts() {
		return parts;
	}

	public void setParts(int[] parts) {
		this.parts = parts;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(super.toString() + ":");
		for (int i = 0; i < this.parts.length; i++) {
			sb.append(this.parts[i]);
			if (i != this.parts.length - 1) {
				sb.append(":");
			}
		}
		return sb.toString();
	}

	public static class RPGReferenceParser extends POReference.ReferenceParser {
		@Override
		public POReference parse(String source) {
			RPGReference ref = new RPGReference();
			final String[] parts = source.split(Pattern.quote(":"));
			if (parts.length != 6) {
				throw new IllegalArgumentException("Invalid count of ':'");
			}
			ref.setFilePath(parts[0]);
			ref.setLine(Integer.parseInt(parts[1]));

			int[] specificParts = new int[4];
			for (int i = 0; i < 4; i++) {
				specificParts[i] = Integer.parseInt(parts[i + 2]);
			}
			ref.setParts(specificParts);

			String filename = Path.of(ref.getFilePath()).getFileName().toString();
			if ("CommonEvents2.json".equals(filename) || "CommonEvents.json".equals(filename)) {
				ref.setType(ReferenceType.COMMON_EVENTS);
			} else if (RPGConstants.PATTERN_MAP.matcher(filename).matches()) {
				ref.setType(ReferenceType.MAP);
			} else {
				throw new IllegalArgumentException("Don't know how to parse reference for file: " + ref.getFilePath());
			}
			return ref;
		}
	}

	public enum ReferenceType {
		MAP, COMMON_EVENTS;
	}
}
