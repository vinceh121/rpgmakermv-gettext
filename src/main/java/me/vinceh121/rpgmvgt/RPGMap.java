package me.vinceh121.rpgmvgt;

import java.util.List;

public class RPGMap {
	private boolean autoplayBgm, autoplayBgs, disableDashing, parallaxLoopX, parallaxLoopY, parallaxShow,
			specifyBattleback;
	private String battleback1Name, battleback2Name, displayName, note, parallaxName;
	private int encounterStep, height, parallaxSx, parallaxSy, scrollType, tilesetId, width;
	private Object bgm, bgs, encounterList;
	private List<Integer> data;
	private List<RPGMapEventGroup> events;

	public boolean isAutoplayBgm() {
		return this.autoplayBgm;
	}

	public void setAutoplayBgm(final boolean autoplayBgm) {
		this.autoplayBgm = autoplayBgm;
	}

	public boolean isAutoplayBgs() {
		return this.autoplayBgs;
	}

	public void setAutoplayBgs(final boolean autoplayBgs) {
		this.autoplayBgs = autoplayBgs;
	}

	public boolean isDisableDashing() {
		return this.disableDashing;
	}

	public void setDisableDashing(final boolean disableDashing) {
		this.disableDashing = disableDashing;
	}

	public boolean isParallaxLoopX() {
		return this.parallaxLoopX;
	}

	public void setParallaxLoopX(final boolean parallaxLoopX) {
		this.parallaxLoopX = parallaxLoopX;
	}

	public boolean isParallaxLoopY() {
		return this.parallaxLoopY;
	}

	public void setParallaxLoopY(final boolean parallaxLoopY) {
		this.parallaxLoopY = parallaxLoopY;
	}

	public boolean isParallaxShow() {
		return this.parallaxShow;
	}

	public void setParallaxShow(final boolean parallaxShow) {
		this.parallaxShow = parallaxShow;
	}

	public boolean isSpecifyBattleback() {
		return this.specifyBattleback;
	}

	public void setSpecifyBattleback(final boolean specifyBattleback) {
		this.specifyBattleback = specifyBattleback;
	}

	public String getBattleback1Name() {
		return this.battleback1Name;
	}

	public void setBattleback1Name(final String battleback1Name) {
		this.battleback1Name = battleback1Name;
	}

	public String getBattleback2Name() {
		return this.battleback2Name;
	}

	public void setBattleback2Name(final String battleback2Name) {
		this.battleback2Name = battleback2Name;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public void setDisplayName(final String displayName) {
		this.displayName = displayName;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(final String note) {
		this.note = note;
	}

	public String getParallaxName() {
		return this.parallaxName;
	}

	public void setParallaxName(final String parallaxName) {
		this.parallaxName = parallaxName;
	}

	public int getEncounterStep() {
		return this.encounterStep;
	}

	public void setEncounterStep(final int encounterStep) {
		this.encounterStep = encounterStep;
	}

	public int getHeight() {
		return this.height;
	}

	public void setHeight(final int height) {
		this.height = height;
	}

	public int getParallaxSx() {
		return this.parallaxSx;
	}

	public void setParallaxSx(final int parallaxSx) {
		this.parallaxSx = parallaxSx;
	}

	public int getParallaxSy() {
		return this.parallaxSy;
	}

	public void setParallaxSy(final int parallaxSy) {
		this.parallaxSy = parallaxSy;
	}

	public int getScrollType() {
		return this.scrollType;
	}

	public void setScrollType(final int scrollType) {
		this.scrollType = scrollType;
	}

	public int getTilesetId() {
		return this.tilesetId;
	}

	public void setTilesetId(final int tilesetId) {
		this.tilesetId = tilesetId;
	}

	public int getWidth() {
		return this.width;
	}

	public void setWidth(final int width) {
		this.width = width;
	}

	public Object getBgm() {
		return this.bgm;
	}

	public void setBgm(final Object bgm) {
		this.bgm = bgm;
	}

	public Object getBgs() {
		return this.bgs;
	}

	public void setBgs(final Object bgs) {
		this.bgs = bgs;
	}

	public Object getEncounterList() {
		return this.encounterList;
	}

	public void setEncounterList(final Object encounterList) {
		this.encounterList = encounterList;
	}

	public List<Integer> getData() {
		return this.data;
	}

	public void setData(final List<Integer> data) {
		this.data = data;
	}

	public List<RPGMapEventGroup> getEvents() {
		return this.events;
	}

	public void setEvents(final List<RPGMapEventGroup> events) {
		this.events = events;
	}

	@Override
	public String toString() {
		return "RPGMap [autoplayBgm="
				+ this.autoplayBgm
				+ ", autoplayBgs="
				+ this.autoplayBgs
				+ ", disableDashing="
				+ this.disableDashing
				+ ", parallaxLoopX="
				+ this.parallaxLoopX
				+ ", parallaxLoopY="
				+ this.parallaxLoopY
				+ ", parallaxShow="
				+ this.parallaxShow
				+ ", specifyBattleback="
				+ this.specifyBattleback
				+ ", battleback1Name="
				+ this.battleback1Name
				+ ", battleback2Name="
				+ this.battleback2Name
				+ ", displayName="
				+ this.displayName
				+ ", note="
				+ this.note
				+ ", parallaxName="
				+ this.parallaxName
				+ ", encounterStep="
				+ this.encounterStep
				+ ", height="
				+ this.height
				+ ", parallaxSx="
				+ this.parallaxSx
				+ ", parallaxSy="
				+ this.parallaxSy
				+ ", scrollType="
				+ this.scrollType
				+ ", tilesetId="
				+ this.tilesetId
				+ ", width="
				+ this.width
				+ ", bgm="
				+ this.bgm
				+ ", bgs="
				+ this.bgs
				+ ", encounterList="
				+ this.encounterList
				+ ", data="
				+ this.data
				+ ", events="
				+ this.events
				+ "]";
	}
}
