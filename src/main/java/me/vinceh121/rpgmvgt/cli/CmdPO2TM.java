package me.vinceh121.rpgmvgt.cli;

import java.io.File;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

import me.vinceh121.gettext.POEntry;
import me.vinceh121.gettext.POReader;
import me.vinceh121.rpgmvgt.RPGReference;
import me.vinceh121.rpgmvgt.TranslationMemoryEntry;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Command(name = "po2tm")
public class CmdPO2TM implements Callable<Integer> {
	@Option(names = { "-i", "--input" })
	private File input;

	@Option(names = { "-o", "--output" })
	private File output;

	@Option(names = { "--ignore-equals" }, description = { "Ignore strings where the source is equal to the target" })
	private boolean ignoreEquals;

	@Option(names = { "--source-language" })
	private String sourceLanguage;

	@Option(names = { "--target-language" })
	private String targetLanguage;

	@Override
	public Integer call() throws Exception {
		List<TranslationMemoryEntry> entries = new LinkedList<>();

		try (FileReader in = new FileReader(this.input)) {
			POReader reader = new POReader(in);
			reader.setReferenceParser(new RPGReference.RPGReferenceParser());

			POEntry poEntry;
			while ((poEntry = reader.nextEntry()) != null) {
				TranslationMemoryEntry tmEntry = new TranslationMemoryEntry();

				tmEntry.setSource(poEntry.getMsgId());
				tmEntry.setTarget(poEntry.getMsgStr());

				tmEntry.setSourceLanguage(this.sourceLanguage);
				tmEntry.setTargetLanguage(this.targetLanguage);

				if (this.ignoreEquals && tmEntry.getSource().equals(tmEntry.getTarget())) {
					continue;
				}

				entries.add(tmEntry);
			}
		}

		RpgCli.MAPPER.writeValue(this.output, entries);

		return 0;
	}
}
