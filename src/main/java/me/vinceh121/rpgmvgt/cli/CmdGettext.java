package me.vinceh121.rpgmvgt.cli;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.Callable;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;

import me.vinceh121.gettext.POEntry;
import me.vinceh121.gettext.POWriter;
import me.vinceh121.rpgmvgt.MVTemplateModelConverter;
import me.vinceh121.rpgmvgt.RPGConstants;
import me.vinceh121.rpgmvgt.RPGEvent;
import me.vinceh121.rpgmvgt.RPGMap;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Command(name = "gettext")
public class CmdGettext implements Callable<Integer> {
	@Option(names = { "-i", "--input-folder" })
	private File inputFolder;

	@Option(names = { "-o", "--output" }, defaultValue = "./out.po")
	private File outputFile;

	@Option(names = { "-l", "--language" }, defaultValue = "en")
	private String language;

	@Option(names = { "-t", "--template" }, defaultValue = "false")
	private boolean template;

	@Override
	public Integer call() throws Exception {
		final List<POEntry> entries = new Vector<>();

		for (final File file : this.inputFolder.listFiles()) {
			final MVTemplateModelConverter conv = this.processFile(file);
			if (conv == null) {
				continue;
			}

			entries.addAll(conv.getEntries());
		}

		final POWriter writer = new POWriter(this.outputFile);
		for (final POEntry e : entries) {
			writer.writeEntry(e);
		}
		writer.close();

		return 0;
	}

	public MVTemplateModelConverter processFile(final File file)
			throws JsonParseException, JsonMappingException, IOException {
		final MVTemplateModelConverter conv = new MVTemplateModelConverter();
		conv.setFilename(file.getPath());
		conv.setIgnoreUntranslatableCodes(true);
		conv.setTemplate(this.template);

		if ("CommonEvents2.json".equals(file.getName()) || "CommonEvents.json".equals(file.getName())) {
			final List<RPGEvent> events = RpgCli.MAPPER.readValue(file, new TypeReference<List<RPGEvent>>() {});
			conv.addEventEntries(events);
		} else if (RPGConstants.PATTERN_MAP.matcher(file.getName()).matches()) {
			final RPGMap map = RpgCli.MAPPER.readValue(file, RPGMap.class);
			conv.addGroupsEntries(map.getEvents());
		} else {
			System.out.println("Don't know how to process " + file);
			return null;
		}

		return conv;
	}

}
