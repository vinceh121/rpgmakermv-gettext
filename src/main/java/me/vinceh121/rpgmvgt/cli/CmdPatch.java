package me.vinceh121.rpgmvgt.cli;

import java.io.File;
import java.io.FileReader;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.Callable;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import me.vinceh121.gettext.POEntry;
import me.vinceh121.gettext.POReader;
import me.vinceh121.rpgmvgt.Patcher;
import me.vinceh121.rpgmvgt.RPGConstants;
import me.vinceh121.rpgmvgt.RPGEvent;
import me.vinceh121.rpgmvgt.RPGMap;
import me.vinceh121.rpgmvgt.RPGReference;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Command(name = "patch")
public class CmdPatch implements Callable<Integer> {
	@Option(names = { "-i", "--data-folder" }, defaultValue = "./")
	private File dataFolder;

	@Option(names = { "-o", "--output" })
	private File outputFolder;

	@Option(names = { "-t", "--po-file" }, required = true)
	private File poFile;

	@Override
	public Integer call() throws Exception {
		if (outputFolder == null) {
			outputFolder = dataFolder;
		}
		if (!dataFolder.isDirectory()) {
			System.err.println("data folder isn't a folder");
			return -1;
		}
		if (!outputFolder.isDirectory()) {
			System.err.println("output folder isn't a folder");
			return -1;
		}

		ObjectMapper mapper = new ObjectMapper();

		List<RPGMap> maps = new Vector<>();
		List<RPGEvent> commonEvents, commonEvents2;

		commonEvents = mapper.readValue(dataFolder.toPath().resolve("CommonEvents.json").toFile(),
				new TypeReference<List<RPGEvent>>() {});
		commonEvents2 = mapper.readValue(dataFolder.toPath().resolve("CommonEvents2.json").toFile(),
				new TypeReference<List<RPGEvent>>() {});

		int mapCount = this.dataFolder.list((f, n) -> RPGConstants.PATTERN_MAP.matcher(n).matches()).length;

		for (int i = 1; i <= mapCount; i++) {
			RPGMap map = RpgCli.MAPPER.readValue(dataFolder.toPath().resolve(String.format("Map%03d.json", i)).toFile(),
					RPGMap.class);
			maps.add(map);
		}

		Patcher patcher = new Patcher();
		patcher.setCommonEvents(commonEvents);
		patcher.setCommonEvents2(commonEvents2);
		patcher.setMaps(maps);

		try (POReader reader = new POReader(new FileReader(poFile))) {
			reader.setReferenceParser(new RPGReference.RPGReferenceParser());
			POEntry entry;
			while (!(entry = reader.nextEntry()).equals(new POEntry())) { // TODO make proper null return
				try {
					patcher.applyTranslation(entry);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		mapper.writeValue(this.outputFolder.toPath().resolve("CommonEvents.json").toFile(), commonEvents);
		mapper.writeValue(this.outputFolder.toPath().resolve("CommonEvents2.json").toFile(), commonEvents2);
		for (int i = 0; i < maps.size(); i++) {
			mapper.writeValue(this.outputFolder.toPath().resolve(String.format("Map%03d.json", i + 1)).toFile(),
					maps.get(i));
		}
		return 0;
	}
}
