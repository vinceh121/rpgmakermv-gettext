package me.vinceh121.rpgmvgt.cli;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import picocli.CommandLine;
import picocli.CommandLine.Command;

@Command(name = "rpgmakermv-gettext", subcommands = { CmdGettext.class, CmdExtract.class, CmdExtractDistance.class,
		CmdPatch.class, CmdPO2TM.class, CmdConcatMultiline.class })
public class RpgCli {
	public static final ObjectMapper MAPPER = new ObjectMapper();
	static {
		MAPPER.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	}

	public static void main(final String[] args) {
		System.exit(new CommandLine(new RpgCli()).execute(args));
	}
}
