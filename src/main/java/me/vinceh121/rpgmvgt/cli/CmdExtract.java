package me.vinceh121.rpgmvgt.cli;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.Callable;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;

import me.vinceh121.gettext.POEntry;
import me.vinceh121.gettext.POWriter;
import me.vinceh121.rpgmvgt.MVPointerTranslatedModelConverter;
import me.vinceh121.rpgmvgt.RPGConstants;
import me.vinceh121.rpgmvgt.RPGEvent;
import me.vinceh121.rpgmvgt.RPGMap;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Command(name = "extract")
public class CmdExtract implements Callable<Integer> {
	@Option(names = { "-i", "--input-folder-original" }, required = true)
	private File inputFolderOrig;

	@Option(names = { "-e", "--input-folder-translated" }, required = true)
	private File inputFolderTrans;

	@Option(names = { "-o", "--output" }, defaultValue = "./out.po")
	private File outputFile;

	@Option(names = { "--ignore-mismatched-sizes" })
	private boolean ignoreMismatchedSizes;

	@Option(names = { "--ignore-cjk" }, description = {
			"Ignore untranslated strings including Chinese/Japanese/Korean characters" })
	private boolean ignoreCjk;

	@Override
	public Integer call() throws Exception {
		if (!this.inputFolderOrig.isDirectory()) {
			System.err.println(this.inputFolderOrig + " isn't a directory");
			return -1;
		}
		if (!this.inputFolderTrans.isDirectory()) {
			System.err.println(this.inputFolderTrans + " isn't a directory");
			return -1;
		}

		final List<POEntry> entries = new Vector<>();

		// we look into trans first so we can ignore files that are in orig but not in
		// trans (e.g. files that don't need translation)
		for (final File file : this.inputFolderTrans.listFiles()) {
			try {
				final MVPointerTranslatedModelConverter conv
						= this.processFile(this.inputFolderOrig.toPath().resolve(file.getName()).toFile(), file);
				if (conv == null) {
					continue;
				}

				entries.addAll(conv.getEntries());
			} catch (final IllegalStateException e) {
				new RuntimeException("Failed to process " + file, e).printStackTrace();
			}
		}

		if (this.ignoreCjk) {
			final List<POEntry> toRemove = new Vector<>();
			for (final POEntry e : entries) {
				if (e.getMsgId().equals(e.getMsgStr()) && !RPGConstants.PAT_CJK.matcher(e.getMsgId()).find()) {
					toRemove.add(e);
				}
			}
			entries.removeAll(toRemove);
		}

		final POWriter writer = new POWriter(this.outputFile);
		for (final POEntry e : entries) {
			writer.writeEntry(e);
		}
		writer.close();

		return 0;
	}

	public MVPointerTranslatedModelConverter processFile(final File fileOrig, final File fileTrans)
			throws JsonParseException, JsonMappingException, IOException {
		if (!fileOrig.getName().equals(fileTrans.getName())) {
			throw new IllegalStateException("Mismatched file names: " + fileOrig + ", " + fileTrans);
		}

		final MVPointerTranslatedModelConverter conv = new MVPointerTranslatedModelConverter();
		conv.setFilenameOrig(fileOrig.getPath());
		conv.setFilenameTrans(fileTrans.getPath());
		conv.setIgnoreUntranslatableCodes(true);
		conv.setIgnoreMismatchedListSizes(this.ignoreMismatchedSizes);

		if ("CommonEvents2.json".equals(fileOrig.getName()) || "CommonEvents.json".equals(fileOrig.getName())) {
			final List<RPGEvent> eventsOrig = RpgCli.MAPPER.readValue(fileOrig, new TypeReference<List<RPGEvent>>() {});
			final List<RPGEvent> eventsTrans
					= RpgCli.MAPPER.readValue(fileTrans, new TypeReference<List<RPGEvent>>() {});
			conv.addEventEntries(eventsOrig, eventsTrans);
		} else if (RPGConstants.PATTERN_MAP.matcher(fileOrig.getName()).matches()) {
			final RPGMap mapOrig = RpgCli.MAPPER.readValue(fileOrig, RPGMap.class);
			final RPGMap mapTrans = RpgCli.MAPPER.readValue(fileTrans, RPGMap.class);
			mapOrig.getEvents().removeIf(o -> o == null);
			mapTrans.getEvents().removeIf(o -> o == null);
			conv.addGroupsEntries(mapOrig.getEvents(), mapTrans.getEvents());
		} else {
			System.out.println("Don't know how to process " + fileOrig + ", " + fileTrans);
			return null;
		}

		return conv;
	}

}
