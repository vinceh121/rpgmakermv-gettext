package me.vinceh121.rpgmvgt.cli;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.Callable;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;

import me.vinceh121.rpgmvgt.RPGConstants;
import me.vinceh121.rpgmvgt.RPGEvent;
import me.vinceh121.rpgmvgt.RPGEventStep;
import me.vinceh121.rpgmvgt.RPGMap;
import me.vinceh121.rpgmvgt.RPGMapEventGroup;
import me.vinceh121.rpgmvgt.RPGMapPage;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Command(name = "contact-multiline")
public class CmdConcatMultiline implements Callable<Integer> {
	@Option(names = { "-i", "--input" })
	private File inputFolder;

	@Option(names = { "-o", "--output" })
	private Path outputFolder;

	@Override
	public Integer call() throws Exception {
		for (final File file : this.inputFolder.listFiles()) {
			this.processFile(file);
		}

		return 0;
	}

	private void processFile(final File file) throws JsonParseException, JsonMappingException, IOException {
		if ("CommonEvents2.json".equals(file.getName()) || "CommonEvents.json".equals(file.getName())) {
			final List<RPGEvent> events = RpgCli.MAPPER.readValue(file, new TypeReference<List<RPGEvent>>() {});

			for (RPGEvent evt : events) {
				if (evt != null) {
					this.concats(evt.getList());
				}
			}

			RpgCli.MAPPER.writeValue(outputFolder.resolve(file.getName()).toFile(), events);
		} else if (RPGConstants.PATTERN_MAP.matcher(file.getName()).matches()) {
			final RPGMap map = RpgCli.MAPPER.readValue(file, RPGMap.class);

			for (RPGMapEventGroup grp : map.getEvents()) {
				if (grp != null) {
					for (RPGMapPage page : grp.getPages()) {
						if (page != null) {
							this.concats(page.getList());
						}
					}
				}
			}

			RpgCli.MAPPER.writeValue(outputFolder.resolve(file.getName()).toFile(), map);
		} else {
			System.out.println("Don't know how to process " + file);
			return;
		}
	}

	private void concats(List<RPGEventStep> steps) {
		for (int j = 0; j < steps.size(); j++) {
			RPGEventStep step = steps.get(j);

			if (step.getCode() == 401) {
				j++;
				for (RPGEventStep nextStep = steps.get(j); nextStep.getCode() == 401 && j + 1 < steps.size(); nextStep
						= steps.get(j++)) {
					step.getParameters().set(0, step.getParameters().get(0) + "\n" + nextStep.getParameters().get(0));
					steps.remove(j);
				}
			}
		}
	}
}
