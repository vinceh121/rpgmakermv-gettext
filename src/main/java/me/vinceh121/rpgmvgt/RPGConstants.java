package me.vinceh121.rpgmvgt;

import java.util.regex.Pattern;

public final class RPGConstants {

	public static final Pattern PAT_CJK
	= Pattern.compile("[\\p{InHiragana}\\p{InKatakana}\\p{InHangulSyllables}\\p{InCjkUnifiedIdeographs}]");
	public static final Pattern PATTERN_MAP = Pattern.compile("Map([0-9]+).json");

}
