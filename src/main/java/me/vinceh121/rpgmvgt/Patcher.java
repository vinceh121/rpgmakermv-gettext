package me.vinceh121.rpgmvgt;

import java.nio.file.Path;
import java.util.List;
import java.util.regex.Matcher;

import me.vinceh121.gettext.POEntry;

/**
 * Applies a Gettext translation to a RPGMakerMV game's CommonEvents(2).json and
 * MapXXX.json
 *
 * Expects PO entries to use RPGReference
 */
public class Patcher {
	private List<RPGEvent> commonEvents, commonEvents2;
	private List<RPGMap> maps;

	public void applyTranslation(List<POEntry> entries) {
		for (POEntry entry : entries) {
			this.applyTranslation(entry);
		}
	}

	public void applyTranslation(POEntry entry) {
		if (entry.getReferences().size() == 0) {
			System.err.println("Entry has no references: " + entry);
			return;
		}
		String fileName = Path.of(entry.getReferences().get(0).getFilePath()).getFileName().toString();

		Matcher mapMatcher = RPGConstants.PATTERN_MAP.matcher(fileName);
		if ("CommonEvents2.json".equals(fileName)) {
			this.applyEvents(entry, commonEvents2);
		} else if ("CommonEvents.json".equals(fileName)) {
			this.applyEvents(entry, commonEvents);
		} else if (mapMatcher.matches()) {
			int mapId = Integer.parseInt(mapMatcher.group(1));
			this.applyEventsMap(entry, maps.get(mapId - 1) // map ids are 1-indexed
					.getEvents());
		} else {
			throw new IllegalArgumentException("Don't know how to process file: " + fileName);
		}
	}

	private void applyEventsMap(POEntry entry, List<RPGMapEventGroup> grps) {
		RPGReference ref = (RPGReference) entry.getReferences().get(0);
		RPGMapEventGroup grp = grps.get(ref.getGroup());
		if (grp == null)
			return;
		RPGMapPage page = grp.getPages().get(ref.getPage());
		RPGEventStep step = page.getList().get(ref.getStep());
		this.applyStep(entry, step);
	}

	private void applyEvents(POEntry entry, List<RPGEvent> evts) {
		RPGReference ref = (RPGReference) entry.getReferences().get(0);
		RPGEvent ev = evts.get(ref.getEvent());

		if (ev == null) {
			throw new IllegalArgumentException("No event by ID " + ref.getEvent());
		}

		RPGEventStep step = ev.getList().get(ref.getStep());
		this.applyStep(entry, step);
	}

	private void applyStep(POEntry entry, RPGEventStep step) {
		Object first = step.getParameters().get(0);

		if (first instanceof String) {
			this.applyStepSingleLine(entry, step);
		} else if (first instanceof List) {
			this.applyStepChoices(entry, step);
		} else {
			throw new IllegalArgumentException("Invalid parameter 0 type: " + first);
		}
	}

	private void applyStepChoices(POEntry entry, RPGEventStep step) {
		RPGReference ref = (RPGReference) entry.getReferences().get(0);

		@SuppressWarnings("unchecked") // we don't need to bother to check this
		List<String> choices = (List<String>) step.getParameters().get(0);

		if (!entry.getMsgId().equals(choices.get(ref.getParameter()))) {
			System.err.println("Original Choice strings do not match: `"
					+ choices.get(ref.getParameter())
					+ "` and `"
					+ entry.getMsgId()
					+ "`");
		}

		choices.set(ref.getParameter(), entry.getMsgStr());
	}

	private void applyStepSingleLine(POEntry entry, RPGEventStep step) {
		if (!entry.getMsgId().equals(step.getParameters().get(0))) {
			System.err.println("Original strings do not match: `"
					+ step.getParameters().get(0)
					+ "` and `"
					+ entry.getMsgId()
					+ "`");
		}

		step.getParameters().set(0, entry.getMsgStr());
	}

	public List<RPGEvent> getCommonEvents() {
		return commonEvents;
	}

	public void setCommonEvents(List<RPGEvent> commonEvents) {
		this.commonEvents = commonEvents;
	}

	public List<RPGEvent> getCommonEvents2() {
		return commonEvents2;
	}

	public void setCommonEvents2(List<RPGEvent> commonEvents2) {
		this.commonEvents2 = commonEvents2;
	}

	public List<RPGMap> getMaps() {
		return maps;
	}

	public void setMaps(List<RPGMap> maps) {
		this.maps = maps;
	}

	// private static RPGEvent getEventById(List<RPGEvent> evts, int id) {
	// for (RPGEvent e : evts) {
	// if (e == null)
	// continue;
	// if (e.getId() == id) {
	// return e;
	// }
	// }
	// return null;
	// }
	//
	// private static RPGMapEventGroup getMapEventGroupById(List<RPGMapEventGroup>
	// grps, int id) {
	// for (RPGMapEventGroup g : grps) {
	// if (g == null)
	// continue;
	// if (g.getId() == id) {
	// return g;
	// }
	// }
	// return null;
	// }
}
