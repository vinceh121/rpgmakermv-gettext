package me.vinceh121.rpgmvgt;

import java.util.List;

public class RPGMapEventGroup {
	private int id, x, y;
	private String name, note;
	private List<RPGMapPage> pages;

	public int getId() {
		return this.id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public int getX() {
		return this.x;
	}

	public void setX(final int x) {
		this.x = x;
	}

	public int getY() {
		return this.y;
	}

	public void setY(final int y) {
		this.y = y;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(final String note) {
		this.note = note;
	}

	public List<RPGMapPage> getPages() {
		return this.pages;
	}

	public void setPages(final List<RPGMapPage> pages) {
		this.pages = pages;
	}

	@Override
	public String toString() {
		return "RPGMapEventGroup [id="
				+ this.id
				+ ", x="
				+ this.x
				+ ", y="
				+ this.y
				+ ", name="
				+ this.name
				+ ", note="
				+ this.note
				+ ", pages="
				+ this.pages
				+ "]";
	}
}
