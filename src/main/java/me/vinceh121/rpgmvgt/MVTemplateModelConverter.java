package me.vinceh121.rpgmvgt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import me.vinceh121.gettext.POEntry;
import me.vinceh121.rpgmvgt.RPGReference.ReferenceType;

public class MVTemplateModelConverter {
	/**
	 * 102: choice
	 * 108: comments?
	 * 356: script instructions, here for NameBoxSet
	 * 401: simple character lines
	 * 402: action?
	 */
	public static final List<Integer> TRANSLATABLE_TYPES = Arrays.asList(102, 356, 401);
	// = Arrays.asList(new Integer[] { 0, 101, 102, 108, 111, 115, 117, 121, 122,
	// 126, 221, 222, 224, 225, 230,
	// 231, 232, 234, 235, 241, 242, 250, 355, 356, 401, 402, 404, 411, 412, 655 });

	private final List<POEntry> entries = new Vector<>();

	private String filename = "";
	private boolean ignoreUntranslatableCodes;
	/**
	 * If true, all msgstr values will be empty in order to make a correct pot file.
	 */
	private boolean template;

	public void addGroupsEntries(final List<RPGMapEventGroup> grps) {
		this.entries.addAll(this.buildGroupsEntries(grps));
	}

	public List<POEntry> buildGroupsEntries(final List<RPGMapEventGroup> grps) {
		final List<POEntry> localEntries = new Vector<>();
		for (int i = 0; i < grps.size(); i++) {
			RPGMapEventGroup g = grps.get(i);
			if (g == null)
				continue;
			List<POEntry> newEntries = this.buildGroupEntries(g);
			for (POEntry e : newEntries) {
				((RPGReference) e.getReferences().get(0)).setGroup(i);
			}
			localEntries.addAll(newEntries);
		}
		return localEntries;
	}

	public void addGroupEntries(final RPGMapEventGroup grp) {
		this.entries.addAll(this.buildGroupEntries(grp));
	}

	public List<POEntry> buildGroupEntries(final RPGMapEventGroup grp) {
		final List<POEntry> localEntries = new Vector<>();
		for (int iPage = 0; iPage < grp.getPages().size(); iPage++) {
			final RPGMapPage p = grp.getPages().get(iPage);
			for (int iEvt = 0; iEvt < p.getList().size(); iEvt++) {
				final RPGEventStep evt = p.getList().get(iEvt);
				List<POEntry> entries = this.buildEntries(grp.getName(), grp.getId(), iPage, evt, ReferenceType.MAP);
				for (POEntry e : entries) {
					((RPGReference) e.getReferences().get(0)).setStep(iEvt);
					((RPGReference) e.getReferences().get(0)).setPage(iPage);
				}
				localEntries.addAll(entries);
			}
		}
		return localEntries;
	}

	public void addEventEntries(final List<RPGEvent> sourceList) {
		this.entries.addAll(this.buildEntries(sourceList));
	}

	public List<POEntry> buildEntries(final List<RPGEvent> sourceList) {
		final List<POEntry> localEntries = new Vector<>();
		for (int iEvt = 0; iEvt < sourceList.size(); iEvt++) {
			final RPGEvent evt = sourceList.get(iEvt);
			if (evt == null) {
				continue;
			}
			for (int iStep = 0; iStep < evt.getList().size(); iStep++) {
				final RPGEventStep step = evt.getList().get(iStep);
				// if (!TRANSLATABLE_TYPES.contains(step.getCode()))
				// continue;

				this.addEntry(evt, iEvt, iStep, step, ReferenceType.COMMON_EVENTS);
			}
		}
		return localEntries;
	}

	public void addEntry(final RPGEvent evt, final int id, final int iStep, final RPGEventStep step,
			ReferenceType type) {
		this.entries.addAll(this.buildEventEntries(evt, id, iStep, step, type));
	}

	public List<POEntry> buildEventEntries(final RPGEvent evt, int id, final int iStep, final RPGEventStep step,
			ReferenceType type) {
		return this.buildEntries(evt.getName(), id, iStep, step, type);
	}

	public List<POEntry> buildEntries(final String name, final int id, final int iStep, final RPGEventStep step,
			ReferenceType type) {
		switch (step.getCode()) {
		case 102:
			return this.buildChoiceEntries(name, id, iStep, step, type);
		case 356:
			POEntry nbs = this.buildNameBoxSet(name, id, iStep, step, type);
			if (nbs == null) {
				return List.of();
			} else {
				return List.of(nbs);
			}
		case 401:
			return List.of(this.buildSingleLine(name, id, iStep, step, type));
		default:
			if (!this.ignoreUntranslatableCodes) {
				throw new IllegalStateException("Don't know how to process event code " + step.getCode());
			} else {
				return Collections.emptyList();
			}
		}
	}

	private POEntry buildNameBoxSet(String name, int id, int iStep, RPGEventStep step, ReferenceType type) {
		POEntry entry = this.buildBaseEntry(name, id, iStep, type);
		entry.setMsgId((String) step.getParameters().get(0));

		if (!entry.getMsgId().startsWith("NameBoxSet"))
			return null;

		if (!this.template) {
			entry.setMsgStr(entry.getMsgId());
		} else {
			entry.setMsgStr("");
		}

		return entry;
	}

	private List<POEntry> buildChoiceEntries(final String name, final int id, final int iStep, final RPGEventStep step,
			ReferenceType type) {
		@SuppressWarnings("unchecked")
		final List<String> strs = (List<String>) step.getParameters().get(0);
		final List<POEntry> localEntries = new ArrayList<>(strs.size());
		for (int i = 0; i < strs.size(); i++) {
			final String s = strs.get(i);
			final POEntry entry = this.buildBaseEntry(name, id, iStep, type);
			((RPGReference) entry.getReferences().get(0)).setParameter(i);
			entry.setMsgId(s);

			if (!this.template) {
				entry.setMsgStr(entry.getMsgId());
			} else {
				entry.setMsgStr("");
			}

			localEntries.add(entry);
		}
		return localEntries;
	}

	private POEntry buildSingleLine(final String name, final int id, final int iStep, final RPGEventStep step,
			ReferenceType type) {
		final POEntry entry = this.buildBaseEntry(name, id, iStep, type);
		entry.setMsgId((String) step.getParameters().get(0));

		if (!this.template) {
			entry.setMsgStr(entry.getMsgId());
		} else {
			entry.setMsgStr("");
		}

		return entry;
	}

	// private POEntry buildBaseEntry(RPGMapEventGroup evt, int iPage, int iEvt) {
	// POEntry entry = this.buildBaseEntry(evt.getName(), evt.getId(), iEvt);
	// ((RPGReference) entry.getReferences().get(0)).setPage(iPage);
	// return entry;
	// }
	//
	// private POEntry buildBaseEntry(RPGEvent evt, int iStep) {
	// return this.buildBaseEntry(evt.getName(), evt.getId(), iStep);
	// }

	private POEntry buildBaseEntry(final String name, final int id, final int iStep, ReferenceType type) {
		final POEntry entry = new POEntry();
		entry.setCommentExtracted(name);
		final RPGReference ref = new RPGReference();
		ref.setType(type);
		ref.setFilePath(this.filename);
		ref.setLine(0);
		ref.setEvent(id);
		ref.setStep(iStep);
		entry.getReferences().add(ref);
		return entry;
	}

	public List<POEntry> getEntries() {
		return this.entries;
	}

	public String getFilename() {
		return this.filename;
	}

	public void setFilename(final String filename) {
		this.filename = filename;
	}

	public boolean isIgnoreUntranslatableCodes() {
		return this.ignoreUntranslatableCodes;
	}

	public void setIgnoreUntranslatableCodes(final boolean ignoreUntranslatableCodes) {
		this.ignoreUntranslatableCodes = ignoreUntranslatableCodes;
	}

	public boolean isTemplate() {
		return template;
	}

	public void setTemplate(boolean template) {
		this.template = template;
	}
}
